/**
 * Created by Bensimoha on 25/08/2015.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.sql.ResultSet;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;


public class WordDocument{

    XWPFDocument document = null;
    FileOutputStream out = null;
    String NomDoc = null;

    public WordDocument(ResultSet[] rs) throws Exception { //rs[0] = resultat d'utilisateur, rs[1] = resultat  des materiaux

        document = new XWPFDocument();
        //retourner au debut du resultat apres l'affichage lors du click sur rechercher
        for(ResultSet result: rs){
            result.beforeFirst();
        }
        
        XWPFParagraph utilParagraph = document.createParagraph();
        XWPFRun titreUtil = utilParagraph.createRun();
        titreUtil.setText("Information utilisateur:");
        titreUtil.setBold(true);
        titreUtil.setFontSize(25);
        utilParagraph.setAlignment(ParagraphAlignment.CENTER);
        titreUtil.addBreak();
        
        //info et image d'utilisateur
        XWPFRun runInfo = utilParagraph.createRun();
        runInfo.setFontSize(18);
        while(rs[0].next()){
            NomDoc = rs[0].getString("Nom_util");
            //insertion d'image d'utilisateur
            String NomImage= rs[0].getString("Photo_util");
            try {
                File userImage = new File("C:\\Users\\Bensimoha\\IdeaProjects\\StockInfo\\src\\User_image\\" + NomImage + ".jpg");
                FileInputStream in = new FileInputStream(userImage);
                runInfo.addPicture(in, XWPFDocument.PICTURE_TYPE_JPEG, NomImage, Units.toEMU(100), Units.toEMU(100));
                in.close();
            }catch (Exception ex){
                runInfo.setText("Aucune photo trouvee");
            }
            runInfo.addBreak();
            //insertion des info utilisateur
            runInfo.setText("Nom complet: ");
            runInfo.setText(rs[0].getString("Nom_util"));
            runInfo.addBreak();

            runInfo.setText("Matricule: ");
            runInfo.setText(rs[0].getString("Mat_util"));
            runInfo.addBreak();

            runInfo.setText("Service: ");
            runInfo.setText(rs[0].getString("Serv_util"));
            runInfo.addBreak();

            runInfo.setText("Telephone: ");
            runInfo.setText(rs[0].getString("Tel_util"));
            runInfo.addBreak();
        }

        XWPFParagraph matParagraph = document.createParagraph();
        matParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titreMat = matParagraph.createRun();
        titreMat.setText("Information des materiaux");
        titreMat.setBold(true);
        titreMat.setFontSize(25);
        titreMat.addBreak();

        XWPFTable MatTableau = document.createTable();
        MatTableau.getCTTbl().addNewTblPr().addNewTblW().setW(BigInteger.valueOf(10000));
        XWPFTableRow premiereligne = MatTableau.getRow(0);
        premiereligne.getCell(0).setText("Numero de serie");
        premiereligne.addNewTableCell().setText("Marque");
        premiereligne.addNewTableCell().setText("Type");

        while(rs[1].next()){
            XWPFTableRow tablerow = MatTableau.createRow();
            tablerow.getCell(0).setText(rs[1].getString("Num_serie"));
            tablerow.getCell(1).setText(rs[1].getString("Marque"));
            tablerow.getCell(2).setText(rs[1].getString("Type"));
        }
    }

    public void create() throws Exception{
        try{
            out = new FileOutputStream(new File("C:\\Users\\Bensimoha\\Desktop\\"+NomDoc+".docx"));
            document.write(out);

        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            out.close();
        }

    }

}

/**
 * Created by Bensimoha on 21/07/2015.
 */

import net.miginfocom.swing.MigLayout;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.ExpandVetoException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.*;
import java.util.UUID;
import java.util.Vector;

public class ApplicationStock extends JFrame {

    SqlConnection sq = null;

    public ApplicationStock()
    {
        sq = new SqlConnection();
        Initialisation();
    }

    private void Initialisation()
    {
        setTitle("Gestion du stock informatique");
        setSize(550, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        //panels
        JPanel[] panels = new JPanel[3];
        for(int i=0; i<panels.length; i++)
        {
            panels[i] = new JPanel();
            panels[i].setLayout(new MigLayout());
        }

        //ajoute des textboxs, labels et bouttons
        AjouterManipulation(panels[0]);
        AjouterRecherche(panels[1]);

        panels[2].add(panels[0], "north");
        panels[2].add(panels[1], "south");

        getContentPane().add(panels[2]);

    }

    //FILE pour IMAGE
    File photoEnregistrer = null;
    File photoUtilisateur = null;

    private void AjouterManipulation(JPanel panel)
    {


        //panels
        JPanel materielPanel = new JPanel();
        JPanel UtilisateurPanel = new JPanel();
        JPanel BouttonPanel = new JPanel();
        materielPanel.setLayout(new MigLayout());
        UtilisateurPanel.setLayout(new MigLayout());
        BouttonPanel.setLayout(new MigLayout());

        //FIELDTEXTS
        JTextField Tserie = new JTextField(15);
        JTextField Tmarque = new JTextField(15);
        JTextField Tutlisateur = new JTextField(15);
        JTextField Tservice = new JTextField(15);
        JTextField Ttelephone = new JTextField(15);
        JTextField Tmatricule = new JTextField(15);

        //BUTTONS
        JButton BchoisirImage = new JButton("Choisir");
        JButton Bajouter = new JButton("Ajouter");
        JButton Bmodifier = new JButton("Modifier");
        JButton Bsupprimer = new JButton("Supprimer");

        //combobox
        JComboBox Ctypes = new JComboBox();
        Ctypes.addItem("Imprimante");
        Ctypes.addItem("Poste de travail");
        Ctypes.addItem("PC portable");

        //ajoute des elements dans panel

        materielPanel.add(new JLabel("Numero de serie:"), "cell 0 0");
        materielPanel.add(Tserie, "span 2");
        materielPanel.add(new JLabel("Marque:"), "cell 0 1");
        materielPanel.add(Tmarque, "span 2");
        materielPanel.add(new JLabel("Type: "), "cell 0 2");
        materielPanel.add(Ctypes, "cell 1 2");

        UtilisateurPanel.add(new JLabel("Utilisateur: "), "cell 0 0");
        UtilisateurPanel.add(Tutlisateur, "span 2");
        UtilisateurPanel.add(new JLabel("Matricule: "), "cell 0 1");
        UtilisateurPanel.add(Tmatricule, "span 2");
        UtilisateurPanel.add(new JLabel("Telephone: "), "cell 0 2");
        UtilisateurPanel.add(Ttelephone,"span 2");
        UtilisateurPanel.add(new JLabel("Service: "), "cell 0 3");
        UtilisateurPanel.add(Tservice, "span 2");
        UtilisateurPanel.add(new JLabel("Photo: "), " cell 0 4");
        UtilisateurPanel.add(BchoisirImage, "span 2");

        BouttonPanel.add(Bajouter);
        BouttonPanel.add(Bmodifier);
        BouttonPanel.add(Bsupprimer);

        TitledBorder manipulationBorder = new TitledBorder("Donnees materiel");
        materielPanel.setBorder(manipulationBorder);

        TitledBorder utilisateurBorder = new TitledBorder("Donnees utilisateur");
        UtilisateurPanel.setBorder(utilisateurBorder);

        panel.add(BouttonPanel,"south");
        panel.add(materielPanel, "west");
        panel.add(UtilisateurPanel, "wrap");


        //clique evenements
        Bajouter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String imageNom = UUID.randomUUID().toString();

                sq.Ajouter(Tutlisateur.getText(), Tmatricule.getText(), Ttelephone.getText(), Tservice.getText(),imageNom
                                , Tserie.getText(), Tmarque.getText(), Ctypes.getSelectedItem().toString());
                try{

                    photoEnregistrer = new File("C:\\Users\\Bensimoha\\IdeaProjects\\StockInfo\\src\\User_image\\"+imageNom+".jpg");
                    BufferedImage bf = ImageIO.read(photoUtilisateur);
                    ImageIO.write(bf,"jpg",photoEnregistrer);
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                Tserie.setText("");
                Tmarque.setText("");
                Tutlisateur.setText("");
            }
        });

        Bmodifier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sq.Modifier(Tserie.getText(),Tutlisateur.getText(), Tmatricule.getText(), Ttelephone.getText(), Tservice.getText(),"refimage");
                Tserie.setText("");
                Tmarque.setText("");
                Tutlisateur.setText("");
            }
        });

        Bsupprimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sq.Supprimer(Tserie.getText());
                Tserie.setText("");
                Tmarque.setText("");
                Tutlisateur.setText("");
            }
        });

        BchoisirImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(UtilisateurPanel);

                if(returnVal == JFileChooser.APPROVE_OPTION)
                {

                    photoUtilisateur = fc.getSelectedFile();
                    BchoisirImage.setText(photoUtilisateur.getName());

                }

            }
        });


    }

    //resultats du recherche(utilisateur, materiaux)
    ResultSet[] rs;
    private void AjouterRecherche(JPanel panel)
    {
        JTextField Trecherche = new JTextField(15);
        JButton Brecherche = new JButton("Rechercher");
        JButton Baffichertt = new JButton("Afficher tout");
        JLabel Limage = new JLabel();
        JLabel Lnom = new JLabel();
        JLabel Lmatricule = new JLabel();
        JLabel Ltelephone = new JLabel();
        JLabel Lservice = new JLabel();
        JTable TableauRecherche = new JTable();
        JButton Bword = new JButton("Extraire word");

        panel.add(new JLabel("N� serie / Utilisateur: "));
        panel.add(Trecherche);
        panel.add(Brecherche);
        panel.add(Baffichertt, "wrap");
        panel.add(new JLabel("Photo: "),"cell 1 2");
        panel.add(Limage,"wrap");
        panel.add(new JLabel("Nom complet: "), "cell 1 3");
        panel.add(Lnom,"wrap");
        panel.add(new JLabel("Matricule: "), "cell 1 4");
        panel.add(Lmatricule,"wrap");
        panel.add(new JLabel("Numero de telephone: "), "cell 1 5");
        panel.add(Ltelephone,"wrap");
        panel.add(new JLabel("Service: "), "cell 1 6");
        panel.add(Lservice,"wrap");
        panel.add(new JScrollPane(TableauRecherche), "span 7");
        panel.add(new Label(), "wrap");


        TitledBorder RechercheBorder = new TitledBorder("Recherche des donnees");
        panel.setBorder(RechercheBorder);


        Brecherche.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                rs = sq.Rechercher(Trecherche.getText());
                BufferedImage image = null;
                try {

                    while (rs[0].next()) {
                        try {

                            File userImage = new File("C:\\Users\\Bensimoha\\IdeaProjects\\StockInfo\\src\\User_image\\" + rs[0].getString("Photo_util") + ".jpg");
                            image = ImageIO.read(userImage);
                            Limage.setIcon(new ImageIcon(image.getScaledInstance(100, 100, 50)));

                        } catch (Exception ex) {
                            Limage.setText("Aucune photo");
                        } finally {
                            Lnom.setText(rs[0].getString("Nom_util"));
                            Lmatricule.setText(rs[0].getString("Mat_util"));
                            Lservice.setText(rs[0].getString("Serv_util"));
                            Ltelephone.setText(rs[0].getString("Tel_util"));
                        }
                    }

                    TableauRecherche.setModel(buildTableModel(rs[1]));
                    panel.add(Bword);
                    panel.setBorder(RechercheBorder);
                    setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }



            }
        });

        Baffichertt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultSet rs = sq.RechercherTt();
                try
                {
                    TableauRecherche.setModel(buildTableModel(rs));
                    panel.setBorder(RechercheBorder);
                    Limage.setIcon(new ImageIcon());
                    Lmatricule.setText("N/A");
                    Lnom.setText("N/A");
                    Lservice.setText("N/A");
                    Ltelephone.setText("N/A");
                    setVisible(true);
                }catch(Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });

        Bword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    WordDocument test = new WordDocument(rs);
                    test.create();
                    JOptionPane.showMessageDialog(null, "Document creer sur le bureau!");
                }catch(Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException
    {

        ResultSetMetaData metaData = rs.getMetaData();

        // nom des colonnes
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++)
        {
            columnNames.add(metaData.getColumnName(column));
        }

        // resultats du tableau
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next())
        {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
            {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return new DefaultTableModel(data, columnNames);

    }

    public static void main(String[] args) throws Exception
    {
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                ApplicationStock app = new ApplicationStock();
                app.setVisible(true);
            }
        });

    }
}

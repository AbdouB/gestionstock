/**
 * Created by Bensimoha on 21/07/2015.
 */
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.sql.*;
public class SqlConnection {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3307/stock_informatique";
    private static Connection conn = null;

    public SqlConnection()
    {
        //enregistrement du driver
        try
        {
            Class.forName(JDBC_DRIVER);
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }

        //overture du connexion
        try
        {
            conn = DriverManager.getConnection(DB_URL,"root","ABDOU5991");
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void Ajouter(String nom, String matricule, String tel, String service, String photo
                        , String numSerie, String marque, String type)
    {
        try
        {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("INSERT IGNORE INTO utilisateurs VALUES(\""+nom+"\", \""+matricule+"\",\""+tel+"\", \"service "+service+"\", \""+photo+"\")");
            stmt.executeUpdate("INSERT IGNORE INTO materiels VALUES(\""+numSerie+"\",\""+ marque+"\",\""+type+"\", \""+nom+"\")");
            stmt.executeUpdate("INSERT INTO possession VALUES(\""+nom+"\",\""+numSerie+"\")");

        }catch(Exception ex)
        {
            System.out.println("Erreur d'ajoute");
            ex.printStackTrace();
        }
    }

    public void Modifier(String s, String nom, String matricule, String tel, String service, String photo)
    {
        try
        {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("INSERT IGNORE INTO utilisateurs VALUES(\""+nom+"\", \""+matricule+"\",\""+tel+"\", \"service "+service+"\", \""+photo+"\")");
            stmt.executeUpdate("UPDATE materiels SET Nom_util = \"" + nom + "\"  WHERE Num_serie = \"" + s + "\"");

        }catch(Exception ex)
        {
            System.out.println("Erreur de modification");
            ex.printStackTrace();
        }
    }

    public void Supprimer(String s)
    {
        try
        {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DELETE from possession WHERE Num_serie = \"" + s+"\"");
            stmt.executeUpdate("DELETE from materiels WHERE Num_serie = \"" + s+"\"");

        }catch(Exception ex)
        {
            System.out.println("Erreur de suppression ");
            ex.printStackTrace();
        }
    }

    public ResultSet[] Rechercher(String r)
    {
        ResultSet rs = null;
        ResultSet resUtil = null;
        ResultSet resMat = null;
        String utilisateur = "";
        String serie = "";

        ResultSet[] results = new ResultSet[2];
        try {
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            Statement stmt2 = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery("SELECT * FROM possession WHERE Num_serie= \"" + r + "\" OR Utilisateur =\"" + r + "\"");
            while (rs.next()) {
                utilisateur = rs.getString("Utilisateur");

            }
            resUtil = stmt.executeQuery("SELECT * FROM utilisateurs WHERE Nom_util=\"" + utilisateur + "\"");
            resMat = stmt2.executeQuery("SELECT Num_serie, Marque, Type FROM materiels WHERE Nom_util=\"" + utilisateur + "\" OR Num_serie='"+r+"'");


            results[0] = resUtil;
            results[1] = resMat;

        }catch(Exception ex)
        {
            System.out.println("Erreur de recherche ");
            ex.printStackTrace();
        }
        return results;
    }

    public ResultSet RechercherTt()
    {
        ResultSet rs = null;

        try
        {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM materiels");

        }
        catch(Exception ex)
        {
            System.out.println("Erreur de recherche");
            ex.printStackTrace();
        }

        return rs;
    }


}
